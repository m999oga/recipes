package creational.builder.developpez;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by M999OGA on 19/10/2016.
 */
public class BuilderGofTest {
    @Before
    public void setUp() throws Exception {


    }

    @Test
    public void BuilderTest() throws Exception {
        // Instancie les objets directeur et monteur
        Monteur lMonteurA = new ConcreteMonteurA();
        Directeur lDirecteurA = new Directeur(lMonteurA);

        Monteur lMonteurB = new ConcreteMonteurB();
        Directeur lDirecteurB = new Directeur(lMonteurB);

        // Appel des différentes méthodes de création
        ObjetComplexe lProduitA = lDirecteurA.creerObjet();
        ObjetComplexe lProduitB = lDirecteurB.creerObjet();

        // Demande l'affichage des valeurs des objets
        // pour visualiser les différences de composition
        lProduitA.afficher();
        lProduitB.afficher();

        // Affichage :
        // Objet Complexe :
        //       - attribut1 : libelle de l'objet (avec dimension en centimètre)
        //       - attribut2 : 12.0
        //       - classe de l'attribut2 : class java.lang.Float
        // Objet Complexe :
        //       - attribut1 : libelle de l'objet (avec dimension en pouces)
        //       - attribut2 : 30.48
        //       - classe de l'attribut2 : class java.lang.Double

    }

}