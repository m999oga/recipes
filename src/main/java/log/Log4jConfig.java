package log;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.PropertyConfigurator;



/**
 * Comparable to {@link org.springframework.web.util.Log4jConfig}, but will be able to read the log4j.properties
 * (or equivalent) file from an <b>unexpanded</b> war. The name of the properties file can be set by providing a value
 * for the servletcontext's initparameter {@link #CONFIG_LOCATION_PARAM}, but note that it can only point to
 * a &lt;someFileName&gt;.properties file, not an xml.
 *
 * LogManager.shutdown() is not used because shutdown is deprecated
 *
 * @author H2R
 * @version $Revision$, $Date$
 */
public class Log4jConfig  {

	private static Log4jConfig l4j = new Log4jConfig();
	private Log4jConfig() {};
	public static Log4jConfig getInstance() {
		return l4j;
	}
    /**
     * Parameter specifying the location of the Log4J config file
     */
    public static final String CONFIG_LOCATION_PARAM = "Log4j.properties";

    public boolean  contextInitialized() throws  IllegalArgumentException,IllegalArgumentException {

        // Only perform custom Log4J initialization in case of a config file.
        String location = CONFIG_LOCATION_PARAM;

        if (location != null) {

            // Interpret location as relative to the web application root directory
            //if (!location.startsWith("/")) {
            //    location = "/" + location;
            //}

            // Write log message to server log.
        	System.out.println("Initializing Log4J from [" + location + "]");
            try {
                Properties props = getPropertyFile(location);
                if (null == props) 
                    return false;
                PropertyConfigurator.configure(props);
            } catch (Exception ex) {
                return false;
            }
        } else
            System.out.println("WARNING!! MSS cannot initialize Log4J logging system; " +
                    "initparam '" + CONFIG_LOCATION_PARAM + "' is missing in web.xml");
        return true;
    }

    private Properties   getPropertyFile(String location)
    {
        Properties properties = null;
        InputStream iStr = null;
        try {
            iStr = this.getClass().getResourceAsStream(location);
        	properties =new Properties();
            properties.load(iStr);
        } catch (Exception ex)  {
            System.err.println("H2R Failed to initialise log4j");
            properties=null;
        } finally {
            try
            {
                if (iStr!=null) iStr.close();
            } catch (IOException e) {
                System.err.println("H2R Failed to close the log4j configuration property stream");
            }
        }
        return properties;
    }

}
