package log;

import log.Log4jConfig;

import org.apache.log4j.Logger;

public class TestLog4J {

    public TestLog4J() {
	super(); 
    }
    
    private static Logger log = Logger.getLogger(TestLog4J.class);
    
	    
    public static void main(String[] args) {
	
	Log4jConfig log4jConfig = Log4jConfig.getInstance();
	if (log4jConfig.contextInitialized()) {
	    log.debug("log4jConfig configured.  YESSSSSSSSSS ");
	} else {
	    log.debug("log4jConfig is not configured !");
	}
	
	//Log logger = new Log();
	log.debug("Hello Log4J");
	log.debug("Bye Bye");
    }
}

