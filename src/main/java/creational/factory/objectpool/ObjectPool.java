package creational.factory.objectpool;
import java.util.ArrayList;

public abstract class ObjectPool
{
  private ArrayList freeItems;
  private ArrayList busyItems;
  private int maxPoolSize=0;
  private int initialPoolSize=0;

  /***** INITIALISATION *****/

  /**
   * Creates a new objectpool.
   *
   * The subclass constructors MUST call fillPool();
   *
   * @param initialPoolSize initial pool size on construction of pool.
   * @param maxPoolSize maximum pool size or 0 if infinite.
   */
  protected ObjectPool(int initialPoolSize,
                       int maxPoolSize) {
    super();
    this.maxPoolSize = maxPoolSize;
    this.initialPoolSize = initialPoolSize;
  }

  protected void fillPool() {
    freeItems = new ArrayList(this.initialPoolSize);
    for (int i=0; i<this.initialPoolSize; i++) {
      freeItems.add(createPooledInstance());
    }
  }

  /**
   * How to create a brand new instance. Must be overwritten
   * by the different subclasses implementing differen pools.
   *
   * NOTE: we could also implement for a kind of call-back
   * method to create new instances or to make a general pool
   * where new pool instances should be aware of a kind of
   * factory. Not considered to be really better for now...
   */
  protected abstract Object createPooledInstance();

  /**
   * Accessor to pool. Will remove a free instance out of the
   * pool or create one if not present.
   *
   * The caller should invoke "releaseInstance()" when it doesn't
   * need the instance anymore.
   */
  public Object getInstance()
  {
    Object instance = null;
    synchronized(freeItems) {
      if (freeItems.size() != 0) instance = freeItems.remove(0);
    }
    if (instance == null) {
		/* freeItems is empty, create one on the spot. */
		instance = createPooledInstance();
	}
    return instance;
  }

  /**
   * Release instance back to pool. If the maximum pool size is not
   * exceeded, it will be added, otherwise it'll be gc'ed.
   */
  public void releaseInstance(Object pooledInstance) {
    synchronized(freeItems) {
      if ((maxPoolSize == 0) ||
          (freeItems.size() < maxPoolSize)) {
        freeItems.add(pooledInstance);
      }
    }
  }
}
