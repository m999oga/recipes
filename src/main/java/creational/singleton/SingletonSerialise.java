package creational.singleton;

import java.io.Serializable;

/*
 * Sérialisation des singletons
 * Pour finir, rappelez-vous qu'il existe une seconde façon d'instancier 
 * des objets : par désérialisation.
 * Si votre Singleton implémente java.io.Serializable, 
 * il faut absolument empêcher que sa désérialisation n'entraîne 
 * la création de nouvelles instances. 
 * 
 * Pour cela, la javadoc indique que la méthode "readResolve()" 
 * permet de remplacer tout objet désérialisé par un objet personnalisé. 
 * Utilisons cela à notre avantage :
 */

public class SingletonSerialise implements Serializable
{	
	/** Constructeur privé */
	private SingletonSerialise()
	{}
 
	/** Instance unique pré-initialisée */
	private static SingletonSerialise INSTANCE = new SingletonSerialise();
 
	/** Point d'accès pour l'instance unique du singleton */
	public static SingletonSerialise getInstance()
	{	return INSTANCE;
	}
 
	/** Sécurité anti-désérialisation */
	private Object readResolve() {
		return INSTANCE;
	}
}