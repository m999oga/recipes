package creational.abstractFactory;

public class ProduitFactory2 implements IProduitFactory{

    @Override
    public AbstractProductA getProduitA() {
	return new ProduitA2();
    }

    @Override
    public AbstractProductB getProduitB() {
	return new ProduitB2();
    }

}
