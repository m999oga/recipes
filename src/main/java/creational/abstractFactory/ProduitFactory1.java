package creational.abstractFactory;

public class ProduitFactory1 implements IProduitFactory {

    @Override
    public AbstractProductA getProduitA() {
	return new ProduitA1();
    }

    @Override
    public AbstractProductB getProduitB() {
	return new ProduitB1();
    }

   

}
