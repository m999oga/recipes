package creational.abstractFactory;

public class ProduitA2 extends AbstractProductA {

    @Override
    public void methodeA() {
	System.out.println("ProduitA2.methodeA()");
    }

}
