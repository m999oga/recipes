package creational.abstractFactory;

public class ProduitB2  extends AbstractProductB {

    @Override
    public void methodeB() {
	System.out.println("ProduitB2.methodeB()");
    }

}
