package structural.decorator.dzone;

public interface Shape {

    void draw();

    void resize();

    String description();

    boolean isHide();

}

