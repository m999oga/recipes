package structural.decorator.usine;

public class ProcessMain {
 	// Implémentation
 	public static void main(String[] args) {
 		DS ds = new DS();
 		DSAvecToitOuvrantDecorator dsOption = new DSAvecToitOuvrantDecorator(ds);
 		System.out.println(dsOption.getPoids()+" - "+dsOption.getPrix());
 	}

}
