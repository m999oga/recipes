package structural.decorator.usine;

public abstract class Voiture {

	String nom, marque; 
	abstract int getPrix();
	abstract int getPoids();
}
