package structural.adapter.media;

public interface MediaPlayer {
    public void play(String audioType, String fileName);
}