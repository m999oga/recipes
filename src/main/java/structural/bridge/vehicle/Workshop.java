package structural.bridge.vehicle;

/**
 * Implementor for bridge pattern
 * */
public interface Workshop {
	abstract public void work();
}

