package structural.bridge.vehicle;

public class ProcessMain {
/*
 * Demonstration of bridge design pattern
 */
    public static void main(String[] args) {

	Vehicle vehicle1 = new Car(new Produce(), new Assemble());
	vehicle1.manufacture();
	Vehicle vehicle2 = new Bike(new Produce(), new Assemble());
	vehicle2.manufacture();

    }
}
/*
Output:
    Car Produced Assembled.
    Bike Produced Assembled.
*/