package behavioral.observer2;

import java.util.Observable;
import java.util.Observer;


/**
 * @author MaxAlex
 * @Date Created on 21 oct. 03
 *
 */

/*
 * Observer
 * 
 * public void update(Observable obs, Object obj)
 * 
 * 	Called when a change has occurred in the state of the observable.
 * 
 */

public abstract class ForcesObserver implements Observer {

	protected int forces=0;

	protected abstract int setValue(ForcesObservable forcesObservable);

	/**
	 * This method is called whenever the observed object is changed. An
	 * application calls an <tt>Observable</tt> object's
	 * <code>notifyObservers</code> method to have all the object's
	 * observers notified of the change.
	 *
	 * @param   o     the observable object.
	 * @param   arg   an argument passed to the <code>notifyObservers</code>
	 *                 method.
	 */
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		this.forces = setValue((ForcesObservable) arg0);
	}
	public int getValue(){
		return this.forces;
	}

}
