package behavioral.strategy.phoenix;

import log.Log4jConfig;

import org.apache.log4j.Logger;


/**
 * @author H2R
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class TestPattern {
    private static Logger log =
        Logger.getLogger(TestPattern.class);

	public static void main(String[] args) {

		Log4jConfig log4jConfig = Log4jConfig.getInstance();
		log4jConfig.contextInitialized();

   		SLBContext slbContext = new SLBContext ("MainDriver");
		try {
			log.debug("SAVE process :" + slbContext.saveScreen ());
		} catch (Exception e) {
			log.debug("Error SAVE process :" + e);
		}

		slbContext.setScreen ("SecondDriver");
		try {
			log.debug("LOAD process :" + slbContext.loadScreen ());
		} catch (Exception e1) {
			log.debug("Error LOAD process :" + e1);

		}


	}
}
