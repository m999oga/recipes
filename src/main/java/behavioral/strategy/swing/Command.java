package behavioral.strategy.swing;

public interface Command
{
   public void Execute();
}
