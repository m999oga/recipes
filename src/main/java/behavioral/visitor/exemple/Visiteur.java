package behavioral.visitor.exemple;

/**
* Définit l'interface d'un visiteur
*/
public interface Visiteur {
    public void visiterElementA(ConcreteElementA pElementA);
    public void visiterElementB(ConcreteElementB pElementB);
}